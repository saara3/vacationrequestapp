# 🏝️ Web app for submitting vacation requests 🏝️

This is a simple React web app that allows employees to automatically submit vacation requests.

The web app has two pages:
- Index page with a list of vacation requests
- Form page for submitting a vacation request

The form has four fields:
- vacation start date
- vacation end date
- number of requested vacation days
- comment

Data is persisted in local storage and obtained over a fake API to mimic a real server.
I chose not to use separate components in this React app due to its small scale. Utilizing a more compact structure without separate components suited to the project's straightforward requirements.

## Setup guide

Ensure that you have Node.js and npm installed. They can be downloaded and installed from the official [Node.js website](https://nodejs.org/en).

Clone the repository:
~~~
git clone https://gitlab.com/saara3/vacationrequestapp
~~~

Navigate into the project directory:
~~~
cd vacationrequestapp
~~~

Run the following command to install the project dependencies:
~~~
npm install
~~~

Start the development server:
~~~
npm start
~~~

The React app should be accessible at http://localhost:3000 by default.
