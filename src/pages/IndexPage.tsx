import React, { useState } from 'react';
import { Table, Pagination } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

interface VacationRequest {
  startDate: Date;
  endDate: Date;
  numberOfDays: number;
  comment: string;
  index: number;
}

interface IndexPageProps {
    vacationRequests: VacationRequest[];
  }

  const formatDate = (date: Date): string => {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-based
    const year = String(date.getFullYear());
  
    return `${day}.${month}.${year}`;
  };

  const ITEMS_PER_PAGE = 20;
  
  const IndexPage: React.FC<IndexPageProps> = ({ vacationRequests }) => {

    const [currentPage, setCurrentPage] = useState(1);

    const lastPageIndex = Math.ceil(vacationRequests.length / ITEMS_PER_PAGE);

    const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;

    const currentRequests = vacationRequests.slice(startIndex, endIndex);

    return (
      <div className="container mt-5">
      <h2 className="mb-4">Vacation Requests</h2>
      <Table bordered hover>
        <thead>
          <tr>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Number of Days</th>
            <th>Comment</th>
          </tr>
        </thead>
        <tbody>
          {currentRequests.map((request, index) => (
            <tr key={index}>
              <td>{formatDate(new Date(request.startDate))}</td>
              <td>{formatDate(new Date(request.endDate))}</td>
              <td>{request.numberOfDays}</td>
              <td>{request.comment}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Pagination>
        <Pagination.Prev
          onClick={() => setCurrentPage((prevPage) => Math.max(prevPage - 1, 1))}
          disabled={currentPage === 1}
        />
        {Array.from({ length: lastPageIndex }).map((_, index) => (
          <Pagination.Item
            key={index + 1}
            active={index + 1 === currentPage}
            onClick={() => setCurrentPage(index + 1)}
          >
            {index + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next
          onClick={() => setCurrentPage((prevPage) => Math.min(prevPage + 1, lastPageIndex))}
          disabled={currentPage === lastPageIndex}
        />
      </Pagination>
    </div>
    );
  
  };
  
  export default IndexPage;
