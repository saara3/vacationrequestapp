import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { VacationRequest } from '../components/types';
import 'bootstrap/dist/css/bootstrap.min.css';

interface FormPageProps {
  onSubmit: (formData: VacationRequest) => void;
}

const FormPage: React.FC<FormPageProps> = ({ onSubmit }) => {
  const navigate = useNavigate();
  
  const [formData, setFormData] = useState<VacationRequest>({
    startDate: new Date(),
    endDate: new Date(),
    numberOfDays: 1,
    comment: '',
    index: 0,
  });

  const [validationErrors, setValidationErrors] = useState({
    startDate: '',
    endDate: '',
    numberOfDays: '',
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
    
    handleDateValidation(name, value);
  };

  const handleTextareaChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    handleInputChange(e);
  };

  const validateStartDate = () => {
    return formData.startDate > formData.endDate ? 'Start date must be before end date' : '';
  };

  const validateEndDate = () => {
    return formData.endDate < formData.startDate ? 'End date must be after start date' : '';
  };

  const validateNumberOfDays = () => {
    return formData.numberOfDays < 1 ? 'Vacation must be at least 1 day long!' : '';
  };

  const handleDateValidation = (name: string, value: string) => {
    const parsedDate = new Date(value);

    if (isNaN(parsedDate.getTime())) {
      setValidationErrors({
        ...validationErrors,
        [name]: 'Invalid date format',
      });
    } else {
      setValidationErrors({
        ...validationErrors,
        [name]: '',
      });
    }
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
  
    const startDateValidation = validateStartDate();
    const endDateValidation = validateEndDate();
    const numberOfDaysValidation = validateNumberOfDays();
  
    if (startDateValidation || endDateValidation || numberOfDaysValidation) {
      setValidationErrors({
        startDate: startDateValidation || '',
        endDate: endDateValidation || '',
        numberOfDays: numberOfDaysValidation || '',
      });
      return;
    }
    
    onSubmit(formData);

    navigate('/');
  };
  const handleCancel = () => {
    navigate('/');
  };

  const calculateEndDate = (startDate: Date, numberOfDays: number) => {
    
    const newDate = new Date(new Date(startDate).getTime() + 
      (numberOfDays - 1) * 24 * 60 * 60 * 1000);

    return newDate;
  };
 

  const calculateNumberOfDays = (startDate: Date, endDate: Date) => {
    
    const timeDifference = new Date(endDate).getTime() - new Date(startDate).getTime();
    const newNumberOfDays = Math.ceil(timeDifference / (1000 * 60 * 60 * 24)) + 1;

    return newNumberOfDays;
  };


  const handleStartDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newStartDate = new Date(e.target.value);
    if(newStartDate.getTime() < new Date().getTime()){
      setValidationErrors((prevErrors) => {
        const updatedErrors = {
          ...prevErrors,
          startDate: "Date cannot be in the past!",
        };
        return updatedErrors;
      });
      return;
    } else {
      setValidationErrors((prevErrors) => ({
        ...prevErrors,
        startDate: "",
      }));
    }

    const newEndDate = calculateEndDate(newStartDate, formData.numberOfDays);

    setFormData((prevData) => {
      const updatedData = {
        ...prevData,
        startDate: newStartDate,
        endDate: newEndDate,
      };
      return updatedData;
    });
  }

  const handleEndDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newEndDate = new Date(e.target.value);
    if(newEndDate.getTime() < new Date().getTime()){
      setValidationErrors((prevErrors) => {
        const updatedErrors = {
          ...prevErrors,
          endDate: "Date cannot be in the past!",
        };
        return updatedErrors;
      });
      return;
    }
    else if(newEndDate.getTime() < formData.startDate.getTime()){
      setValidationErrors((prevErrors) => {
        const updatedErrors = {
          ...prevErrors,
          endDate: "End date must be after start date!",
        };
        return updatedErrors;
      });
      return;
    }
    else {
      setValidationErrors((prevErrors) => ({
        ...prevErrors,
        endDate: "",
      }));
    }

    const newNumberOfDays = calculateNumberOfDays(formData.startDate, newEndDate);
    
    setFormData((prevData) => {
      const updatedData = {
        ...prevData,
        endDate: newEndDate,
        numberOfDays: newNumberOfDays,
      };
      return updatedData;
    });
  }

  const handleNumberOfDaysChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newNumberOfDays = Number(e.target.value);
    if(newNumberOfDays < 1){
      setValidationErrors((prevErrors) => {
        const updatedErrors = {
          ...prevErrors,
          numberOfDays: "Vacation must be at least 1 day long!",
        };
        return updatedErrors;
      });
      return;
    } else {
      setValidationErrors((prevErrors) => ({
        ...prevErrors,
        numberOfDays: "",
      }));
    }
    const newEndDate = calculateEndDate(formData.startDate, newNumberOfDays);
    setFormData((prevData) => {
      const updatedData = {
        ...prevData,
        endDate: newEndDate,
        numberOfDays: newNumberOfDays,
      };
      return updatedData;
    });
  }
  
  return (
    <div className="container mt-5">
      <h2 className="mb-4">Submit a vacation request</h2>
      <form className="" onSubmit={handleSubmit}>
        <div className="row mb-3">
          <label className="col-sm-3 col-form-label">
            Start Date:
          </label>
          <div className="col-sm-3">
            <input
              type="date"
              name="startDate"
              className="form-control"
              value={new Date(formData.startDate).toISOString().split('T')[0]}
              onChange={handleStartDateChange}
              required
            />
            <div className="text-danger">{validationErrors.startDate}</div>
          </div>
        </div>
        <div className="row mb-3">
          <label className="col-sm-3 col-form-label">
            End Date:
          </label>
          <div className="col-sm-3">
            <input
              type="date"
              name="endDate"
              className="form-control"
              value={new Date(formData.endDate).toISOString().split('T')[0]}
              onChange={handleEndDateChange}
            />
            <div className="text-danger">{validationErrors.endDate}</div>
          </div>
        </div>
        <div className="row mb-3">
          <label className="col-sm-3 col-form-label">
            Number of vacation days:
          </label>
          <div className="col-sm-3">
            <input
              type="number"
              name="numberOfDays"
              className="form-control"
              value={formData.numberOfDays}
              onChange={handleNumberOfDaysChange}
              required
            />
            <div className="text-danger">{validationErrors.numberOfDays}</div>
          </div>
        </div>
        <div className="row mb-3">
          <label className="col-sm-3 col-form-label">
            Comment:
          </label>
          <div className="col-sm-3">
            <textarea
              name="comment"
              className="form-control" 
              value={formData.comment}
              onChange={handleTextareaChange}
            />
          </div>
        </div>
        <div className="row mb-3">
          <div className="d-flex flex-sm-row">
            <div className="col-sm-2 offset-sm-3">
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
            <div className="col-sm-2">
              <button type="button" className="btn btn-secondary" onClick={handleCancel}>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default FormPage; 
