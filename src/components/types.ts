export interface VacationRequest {
    startDate: Date;
    endDate: Date;
    numberOfDays: number;
    comment: string;
    index: number;
  }
  