import React from 'react';
import { Link } from 'react-router-dom';


const Navigation: React.FC = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark" style={{ backgroundColor: '#0066cd' }}>
      <div className="container">
        <Link to="/" className="navbar-brand text-white">
          Vacation Request App
        </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/" className="nav-link text-white">
                All Vacations
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/form" className="nav-link text-white">
                New 
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
