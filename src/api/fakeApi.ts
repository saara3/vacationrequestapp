interface VacationRequest {
    startDate: Date;
    endDate: Date;
    numberOfDays: number;
    comment: string;
    index: number;
  }
  
  const fakeData: VacationRequest[] = [
    {startDate: new Date('2023-06-01'),
    endDate: new Date('2023-06-07'),
    numberOfDays: 7,
    comment: 'Taking a week off to recharge.',
    index: 1},
    
    {startDate: new Date('2023-08-18'),
    endDate: new Date('2023-08-21'),
    numberOfDays: 4,
    comment: 'Long weekend getaway with family.',
    index: 2},

    {startDate: new Date('2023-09-15'),
    endDate: new Date('2023-09-15'),
    numberOfDays: 1,
    comment: 'Attending a special event.',
    index: 3}
  ];
  export const initializeLocalStorage = (): void => {
    localStorage.setItem('fakeData', JSON.stringify(fakeData));
  };
  
  export const getRequests = (): VacationRequest[] => {
    const storedData = JSON.parse(localStorage.getItem('fakeData') || '[]') as VacationRequest[];
    return storedData;
  };
  
  export const addRequest = (item: VacationRequest): void => {
    const storedData = getRequests();
    storedData.push(item);
    localStorage.setItem('fakeData', JSON.stringify(storedData));
  };
  
  export const fetchRequests = (): Promise<VacationRequest[]> => {
    return new Promise((resolve) => {
      // Simulate an asynchronous request
      setTimeout(() => {
        const items = getRequests();
        resolve(items);
      }, 500);
    });
  };
  
  export const createRequest = (startDate: Date, endDate: Date, numberOfDays: number, comment: string): Promise<VacationRequest> => {
    return new Promise((resolve) => {
        const existingItems = getRequests();
        const index = existingItems.length > 0 ? existingItems[existingItems.length - 1].index + 1 : 1;
      // Simulate an asynchronous request
      setTimeout(() => {
        const newItem: VacationRequest = { startDate, endDate, numberOfDays, comment, index};
        addRequest(newItem);
        resolve(newItem);
      }, 500);
    });
  };
  