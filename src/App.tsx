import { useState, useEffect } from 'react';
import IndexPage from './pages/IndexPage';
import FormPage from './pages/FormPage';
import { VacationRequest } from './components/types';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navigation from './components/navigation';
import { fetchRequests, createRequest, initializeLocalStorage } from './api/fakeApi';


const App: React.FC = () => {
  const [vacationRequests, setVacationRequests] = useState<VacationRequest[]>([]);

  useEffect(() => {
    initializeLocalStorage();
    // Fetch items from the fake API when the component mounts
    fetchRequests().then((data) => setVacationRequests(data));
  }, []);

  const handleVacationRequestSubmit = (formData: VacationRequest) => {
    const vacationRequest: VacationRequest = {
      ...formData,
      index: vacationRequests.length,
    };
    setVacationRequests([...vacationRequests, vacationRequest]);
    // Add a new item to the fake API
    createRequest(formData.startDate, formData.endDate, formData.numberOfDays, formData.comment).then(() => {
      // Update the state to reflect the new data
      fetchRequests().then((data) => setVacationRequests(data));
    });
  };

  return (
    <BrowserRouter>
    <Navigation />
      <Routes>
        <Route
          path="/"
          element={<IndexPage vacationRequests={vacationRequests} />}
        />
        <Route
          path="/form"
          element={<FormPage onSubmit={handleVacationRequestSubmit} />}
        />
        <Route
          path="/vacation_requests"
          element={<FormPage onSubmit={handleVacationRequestSubmit} />}
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
